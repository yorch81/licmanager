﻿Public Class frmManager
    Private maeasLic As CisaLic = Nothing

    Private Sub frmManager_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        maeasLic = CisaLic.getInstance("https://cisa-lic.firebaseio.com/.json", "IBR4ajE6403T8VbLTWr59e6dLLDe2qGAtcToaFXN")
        'maeasLic = CisaLic.getInstance("http://firemongo.v3ctor.club/", "IBR4ajE6403T8VbLTWr59e6dLLDe2qGAtcToaFXN")

        Dim jsonLic = maeasLic.getLicenses()

        Dim licDt As DataTable = JsonTools.json2DataTable(jsonLic)

        GrdLicenses.DataSource = licDt
    End Sub

    Private Sub cmdEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEditar.Click
        Dim oFrm As New frmLicense(GrdLicenses.CurrentRow.Cells("client").Value, GrdLicenses.CurrentRow.Cells("expires").Value)
        oFrm.ShowDialog()

        If oFrm.getContinue Then
            GrdLicenses.CurrentRow.Cells("client").Value = oFrm.getClient
            GrdLicenses.CurrentRow.Cells("expires").Value = oFrm.getExpires
        End If

        oFrm = Nothing
    End Sub

    Private Sub cmdAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAgregar.Click
        Dim tmpLic As DataTable = GrdLicenses.DataSource

        Dim oFrm As New frmLicense("", "")
        oFrm.ShowDialog()

        If oFrm.getContinue Then
            Dim oRow As DataRow = tmpLic.NewRow
            oRow.Item("client") = oFrm.getClient
            oRow.Item("expires") = oFrm.getExpires

            tmpLic.Rows.Add(oRow)
        End If

        GrdLicenses.DataSource = tmpLic
        oFrm = Nothing
    End Sub

    Private Sub cmdGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardar.Click
        Dim dtLic As DataTable = GrdLicenses.DataSource
        Dim jsonLicenses As String = JsonTools.DataTable2Json(dtLic)

        maeasLic.setLicenses(jsonLicenses)

        Me.Close()
    End Sub

    Private Sub cmdCreditos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCreditos.Click
        Dim oFrm As New Credits

        oFrm.ShowDialog()

        oFrm = Nothing
    End Sub
End Class