﻿' Copyright 2015 Jorge Alberto Ponce Turrubiates
'
' Licensed under the Apache License, Version 2.0 (the "License");
' you may not use this file except in compliance with the License.
' You may obtain a copy of the License at
'
'     http://www.apache.org/licenses/LICENSE-2.0
'
' Unless required by applicable law or agreed to in writing, software
' distributed under the License is distributed on an "AS IS" BASIS,
' WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
' See the License for the specific language governing permissions and
' limitations under the License.

''' <summary>
''' Some Functions for manage JSON
''' </summary>
''' <remarks></remarks>
Public Class JsonTools

    ''' <summary>
    ''' Convert License Structure to DataTable
    ''' </summary>
    ''' <param name="strJson">
    ''' License Structure
    ''' </param>
    ''' <returns>
    ''' DataTable
    ''' </returns>
    ''' <remarks></remarks>
    Public Shared Function json2DataTable(ByVal strJson As String) As DataTable
        Dim oDatatable As New DataTable
        Dim oColumn As New DataColumn

        oColumn.ReadOnly = False
        oColumn.ColumnName = "client"
        oColumn.DataType = System.Type.GetType("System.String")
        oDatatable.Columns.Add(oColumn)

        oColumn = New DataColumn
        oColumn.ReadOnly = False
        oColumn.ColumnName = "expires"
        oColumn.DataType = System.Type.GetType("System.String")
        oDatatable.Columns.Add(oColumn)

        If strJson.Length > 0 Then
            Dim oRow As DataRow = oDatatable.NewRow()

            strJson = strJson.Replace("""", "")
            strJson = strJson.Replace("{", "")
            strJson = strJson.Replace("}", "")

            Dim strClientes() As String = strJson.Split(",")

            For i = 0 To strClientes.Length - 1
                Dim strLic() = strClientes(i).Split(":")

                oRow = oDatatable.NewRow()
                oRow.Item("client") = strLic(0)
                oRow.Item("expires") = strLic(1).Replace("|", "/")

                oDatatable.Rows.Add(oRow)
            Next

            oDatatable.TableName = "JSON"
        End If

        Return oDatatable
    End Function

    ''' <summary>
    ''' Convert License DataTable to Json Object
    ''' </summary>
    ''' <param name="jsonDt">Valid DataTable</param>
    ''' <returns>JSON String</returns>
    ''' <remarks></remarks>
    Public Shared Function DataTable2Json(ByVal jsonDt As DataTable) As String
        Dim retValue As String = ""

        If Not jsonDt Is Nothing Then
            retValue = "{"

            For i = 0 To jsonDt.Rows.Count - 1
                retValue = retValue & """" & jsonDt.Rows(i).Item("client") & """:""" & jsonDt.Rows(i).Item("expires").ToString.Replace("/", "|") & ""","
            Next

            retValue = retValue.Substring(0, retValue.Length - 1)

            retValue = retValue & "}"
        End If

        Return retValue
    End Function

End Class
