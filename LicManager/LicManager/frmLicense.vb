﻿Public Class frmLicense
    Private _Client As String
    Private _Expires As String
    Private _Continue As Boolean = False

    Public Sub New(ByVal strClient As String, ByVal strExpires As String)
        _Client = strClient
        _Expires = strExpires

        InitializeComponent()
    End Sub

    Private Sub frmLicense_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If _Client <> "" Then
            txtClient.Enabled = False
        End If

        txtClient.Text = _Client

        Dim myDate As Date

        If _Expires <> "" Then
            Dim strDate() As String = _Expires.Split("/")

            myDate = New Date(Integer.Parse(strDate(0)), Integer.Parse(strDate(1)), Integer.Parse(strDate(2)))
        Else
            myDate = Date.Now
        End If

        DateExpires.Value = myDate
    End Sub

    Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOk.Click
        _Continue = True
        Me.Close()
    End Sub

    Public Function getClient() As String
        Return txtClient.Text
    End Function

    Public Function getExpires() As String
        Dim retValue As String = ""
        Dim myDate As Date = DateExpires.Value

        retValue = myDate.Year.ToString & "/" & myDate.Month.ToString & "/" & myDate.Day.ToString

        Return retValue
    End Function

    Public Function getContinue() As Boolean
        Return _Continue
    End Function
End Class