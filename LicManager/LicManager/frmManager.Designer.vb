﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmManager
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmManager))
        Me.GrdLicenses = New System.Windows.Forms.DataGridView()
        Me.Client = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.expires = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdEditar = New System.Windows.Forms.Button()
        Me.cmdAgregar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdGuardar = New System.Windows.Forms.Button()
        Me.cmdCreditos = New System.Windows.Forms.Button()
        CType(Me.GrdLicenses, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GrdLicenses
        '
        Me.GrdLicenses.AllowUserToAddRows = False
        Me.GrdLicenses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdLicenses.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Client, Me.expires})
        Me.GrdLicenses.Location = New System.Drawing.Point(35, 60)
        Me.GrdLicenses.Name = "GrdLicenses"
        Me.GrdLicenses.ReadOnly = True
        Me.GrdLicenses.Size = New System.Drawing.Size(293, 136)
        Me.GrdLicenses.TabIndex = 0
        '
        'Client
        '
        Me.Client.DataPropertyName = "client"
        Me.Client.HeaderText = "CLIENTE"
        Me.Client.Name = "Client"
        Me.Client.ReadOnly = True
        Me.Client.Width = 120
        '
        'expires
        '
        Me.expires.DataPropertyName = "expires"
        Me.expires.HeaderText = "EXPIRA"
        Me.expires.Name = "expires"
        Me.expires.ReadOnly = True
        Me.expires.Width = 130
        '
        'cmdEditar
        '
        Me.cmdEditar.Location = New System.Drawing.Point(23, 211)
        Me.cmdEditar.Name = "cmdEditar"
        Me.cmdEditar.Size = New System.Drawing.Size(76, 32)
        Me.cmdEditar.TabIndex = 1
        Me.cmdEditar.Text = "Editar"
        Me.cmdEditar.UseVisualStyleBackColor = True
        '
        'cmdAgregar
        '
        Me.cmdAgregar.Location = New System.Drawing.Point(103, 211)
        Me.cmdAgregar.Name = "cmdAgregar"
        Me.cmdAgregar.Size = New System.Drawing.Size(76, 32)
        Me.cmdAgregar.TabIndex = 2
        Me.cmdAgregar.Text = "Agregar"
        Me.cmdAgregar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(103, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(156, 24)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "License Manager"
        '
        'cmdGuardar
        '
        Me.cmdGuardar.Location = New System.Drawing.Point(184, 211)
        Me.cmdGuardar.Name = "cmdGuardar"
        Me.cmdGuardar.Size = New System.Drawing.Size(76, 32)
        Me.cmdGuardar.TabIndex = 5
        Me.cmdGuardar.Text = "Guardar"
        Me.cmdGuardar.UseVisualStyleBackColor = True
        '
        'cmdCreditos
        '
        Me.cmdCreditos.Location = New System.Drawing.Point(265, 211)
        Me.cmdCreditos.Name = "cmdCreditos"
        Me.cmdCreditos.Size = New System.Drawing.Size(76, 32)
        Me.cmdCreditos.TabIndex = 6
        Me.cmdCreditos.Text = "Créditos"
        Me.cmdCreditos.UseVisualStyleBackColor = True
        '
        'frmManager
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(363, 280)
        Me.Controls.Add(Me.cmdCreditos)
        Me.Controls.Add(Me.cmdGuardar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdAgregar)
        Me.Controls.Add(Me.cmdEditar)
        Me.Controls.Add(Me.GrdLicenses)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmManager"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "License Manager"
        CType(Me.GrdLicenses, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GrdLicenses As System.Windows.Forms.DataGridView
    Friend WithEvents cmdEditar As System.Windows.Forms.Button
    Friend WithEvents cmdAgregar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Client As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents expires As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdGuardar As System.Windows.Forms.Button
    Friend WithEvents cmdCreditos As System.Windows.Forms.Button
End Class
