﻿Imports System.Net
Imports System.IO

' Copyright 2015 Jorge Alberto Ponce Turrubiates
'
' Licensed under the Apache License, Version 2.0 (the "License");
' you may not use this file except in compliance with the License.
' You may obtain a copy of the License at
'
'     http://www.apache.org/licenses/LICENSE-2.0
'
' Unless required by applicable law or agreed to in writing, software
' distributed under the License is distributed on an "AS IS" BASIS,
' WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
' See the License for the specific language governing permissions and
' limitations under the License.

''' <summary>
''' Manage expiration Dates for Atlantus Systems
''' </summary>
''' <remarks></remarks>
Public Class CisaLic
    ''' <summary>
    ''' Instance for Manage Singleton
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared INSTANCE As CisaLic = Nothing

    ''' <summary>
    ''' Url of License WebService
    ''' </summary>
    ''' <remarks></remarks>
    Private URL As String = ""

    ''' <summary>
    ''' Private Constructor of the Class
    ''' </summary>
    ''' <param name="url">
    ''' Url of License WebService
    ''' </param>
    ''' <param name="keyAuth">
    ''' Authorization Key
    ''' </param>
    ''' <remarks></remarks>
    Private Sub New(ByVal url As String, ByVal keyAuth As String)
        Me.URL = url & "?auth=" & keyAuth
    End Sub

    ''' <summary>
    ''' Singleton Implementation of Constructor
    ''' </summary>
    ''' <param name="url">
    ''' Url of License WebService
    ''' </param>
    ''' <param name="keyAuth">
    ''' Authorization Key
    ''' </param>
    ''' <remarks></remarks>
    Public Shared Function getInstance(ByVal url As String, ByVal keyAuth As String) As CisaLic
        If INSTANCE Is Nothing Then
            INSTANCE = New CisaLic(url, keyAuth)
        End If

        Return INSTANCE
    End Function

    ''' <summary>
    ''' Gets a JSON of a Licenses
    ''' </summary>
    ''' <returns>Json Structure</returns>
    ''' <remarks></remarks>
    Public Function getLicenses() As String
        Dim retValue As String = ""

        Dim request As HttpWebRequest
        Dim response As HttpWebResponse = Nothing
        Dim reader As StreamReader

        Try
            request = DirectCast(WebRequest.Create(Me.URL), HttpWebRequest)

            response = DirectCast(request.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            retValue = reader.ReadToEnd()

            If retValue = "null" Then
                retValue = ""
            End If
        Finally
            If Not response Is Nothing Then response.Close()
        End Try

        Return retValue
    End Function

    ''' <summary>
    ''' Sets a JSON of a Licenses in a FireBase DataBase
    ''' </summary>
    ''' <param name="strLic">JSON Structure of Licenses</param>
    ''' <returns>True if successful</returns>
    ''' <remarks></remarks>
    Public Function setLicenses(ByVal strLic As String) As Boolean
        Dim retValue As Boolean = True

        Dim request As HttpWebRequest
        Dim response As HttpWebResponse = Nothing
        Dim reader As StreamReader
        Dim cMessage As String = ""

        Try
            request = DirectCast(WebRequest.Create(Me.URL), HttpWebRequest)

            Dim putData As String = strLic

            Dim data As Byte() = System.Text.Encoding.UTF8.GetBytes(putData)

            request.Method = "PUT"
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = data.Length

            Dim stream As Stream = request.GetRequestStream()
            stream.Write(data, 0, data.Length)
            stream.Close()

            response = DirectCast(request.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            cMessage = reader.ReadToEnd()
        Catch
            retValue = False
        Finally
            If Not response Is Nothing Then response.Close()
            retValue = False
        End Try

        Return retValue
    End Function
End Class