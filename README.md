# LicManager #

## Description ##
Administrador de Vigencias de Licencias para el Sistema Atlantus.

## Requirements ##
* [.NET Framework](http://www.microsoft.com/es-mx/download/details.aspx?id=30653)
* [FireBase](https://www.firebase.com/)

## Developer Documentation ##
In the Code.

## Installation ##
Run EXE file.

## Notes ##
Esta aplicación usa una cuenta de Firebase para administrar las vigencias de las licencias.

## References ##
http://es.wikipedia.org/wiki/Singleton




